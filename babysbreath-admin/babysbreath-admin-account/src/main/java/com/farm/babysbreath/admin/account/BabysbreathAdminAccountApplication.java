package com.farm.babysbreath.admin.account;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BabysbreathAdminAccountApplication {

    public static void main(String[] args) {
        SpringApplication.run(BabysbreathAdminAccountApplication.class, args);
    }

}
