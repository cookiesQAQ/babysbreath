package com.farm.babysbreath.admin.account.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.farm.babysbreath.admin.account.dao.persist.mapper")
public class MyBatisConfiguration {
}
