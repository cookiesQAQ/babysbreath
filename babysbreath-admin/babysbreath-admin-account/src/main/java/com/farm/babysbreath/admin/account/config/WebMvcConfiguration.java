package com.farm.babysbreath.admin.account.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**") //针对某些请求路径进行跨域访问的配置
                .allowedOriginPatterns("*") //允许客户端来自哪里
                .allowedHeaders("*") //允许跨域访问的请求中请求头如何配置
                .allowedMethods("*") //允许跨域访问的请求类型
                .allowCredentials(true) //允许协议凭证
                .maxAge(3600);
    }
}
