package com.example.babysbreath.mall.account;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BabysbreathMallAccountApplication {

    public static void main(String[] args) {
        SpringApplication.run(BabysbreathMallAccountApplication.class, args);
    }

}
