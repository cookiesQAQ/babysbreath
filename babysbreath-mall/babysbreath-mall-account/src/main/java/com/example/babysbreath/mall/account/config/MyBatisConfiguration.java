package com.example.babysbreath.mall.account.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.example.babysbreath.mall.account.dao.persist.mapper")
public class MyBatisConfiguration {
}
